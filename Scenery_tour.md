# Guide to demo scenery

The current demonstration scenery encompasses a 2x2 degree area around Sydney,
Australia.  It includes:

* the latest YSSY and YKAT layouts from the XPlane scenery gateway
* cliffs generated using the latest cliff creation tools in terragear
* high-resolution landcovers generated from OSM and Australian government land use
and vegetation data

The most realistic view will need very high random vegetation density, however
this will hit your framerate pretty hard.

# Flight suggestion

Australian procedure charts available from 
http://www.airservicesaustralia.com/AIP/current/dap/AeroProcChartsTOC.htm

* Depart YSSY heading south along the coast at a few thousand feet. Enjoy the 
view of the coastal cliffs and Royal National Park, the second oldest national
park in the world after Yellowstone. Land at Shellharbour airport (YSHL), noting
the height minimums to the west due to the escarpment. Note that YSHL was recently 
renamed from YWOL (Wollongong), and FlightGear is still using the old name.

* From YSHL take your light aircraft to Katoomba airstrip in the Blue 
Mountains (YKAT). This is used by firefighting aircraft, particularly at
the moment.  Practice a few circuits, enjoying the views. A lot of this
bush was burnt out in the 2019-20 bushfires.

* Fly across to Camden (YSCN) a small GA airport that is a hive of activity
on the weekend, with flight schools, gliders, and even fighter jet joyrides.
On the way note Warragamba dam, which is Sydney's main water supply and only
44% full at the moment (that value not included in the scenery!).

* Then return across Sydney to YSSY, unless you prefer to hangar your aircraft
at YSCN

# Other activities

* Take a joyride from Bankstown airport (YSBK) along the harbour to the ocean,
returning via Botany Bay and the Georges River.

* Your discoveries here...
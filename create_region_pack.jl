# Create a region pack including only those textures
# that are used in the xml files.

#== Instructions ==

1. Set xml_files to the names of the materials files containing the textures
   that you want to include (without the xml extension)

2. Set top_dir to the directory containing the textures and regions directories
3. Set canon_dir to the flightgear directory with stock textures
3. Run make_tar_file() at the Julia prompt. The file will be created in top_dir.

==#

const xml_files = ["australia_mediterranean_summer",
                   "australia_mediterranean_winter",
                   "australia_monsoonal",
                   "australia_new_england",
                   "australia_temperate_coastal_winter",
                   "australia_temperate_inland_winter",
                   "australia_temperate_coastal",
                   "australia_temperate_inland",
                   "australia"]
 
const top_dir = "/home/jrh/programs/Games/FlightGear/CustomScenery/australian_scenery"
const canon_dir = "/home/jrh/programs/Games/FlightGear/next/fgdata/Textures"
const texture_dir = joinpath(top_dir,"textures")
const mat_dir = joinpath(top_dir,"regions")
const all_mats = map(x -> joinpath(top_dir,mat_dir,x*".xml"),xml_files)

using LibExpat

get_all_files() = begin
    texture_locs = ["material//texture","material//tree-texture","material//object-mask"]
    all_textures = []
    for m in all_mats
        mxpat = xp_parse(read(m,String))
        for t in texture_locs
            raw_textures = xpath(mxpat,t)
            raw_textures = map(x->x.elements[1],raw_textures)
            append!(all_textures,raw_textures)
        end
    end
    return unique(all_textures)
end

# Check
get_needed_files() = begin
    all_required = get_all_files()
    filter!(x->!isfile(joinpath(canon_dir,x))||islink(joinpath(canon_dir,x)),all_required)
    return all_required
end

is_file_used(fname) = begin
    for m in all_mats
        try
            run(`grep $fname $m`)
        catch e
            continue
        end
        return true
    end
    return false
end

get_file_list() = begin
    file_list = []
    for d in ["Terrain","Trees"]
        full_dir = joinpath(top_dir,texture_dir,d)
        append!(file_list,map(x->joinpath(d,x),readdir(full_dir)))
    end
    println("All texture files")
    return file_list
end

make_tar_file() = begin
    all_files = map(x->joinpath(texture_dir,x),get_needed_files())
    not_here = filter(x->!isfile(x),all_files)
    if length(not_here) > 0
        println("Error! Following files not available:")
        for n in not_here
            println("$n")
        end
        return
    end
    all_files = map(x->joinpath("textures",x),get_needed_files())
    mat_files = map(x->joinpath("regions",x*".xml"),xml_files)
    append!(all_files,mat_files)
    push!(all_files,joinpath("regions","materials.xml"))
    cur_dir = pwd()
    cd(top_dir)
    run(`tar cvzf region_pack.tgz $all_files`)
    cd(cur_dir)
end

if abspath(PROGRAM_FILE) == @__FILE__
    make_tar_file()
end
